import "./App.css";

import Component from "./components/Component";
import CounterA from "./components/CounterA";
import CounterB from "./components/CounterB";
import StateXA from "./components/StateXA";
import StateXB from "./components/StateXB";
import StateYA from "./components/StateYA";
import StateYB from "./components/StateYB";

export default function Primitives() {
  return (
    <main className="primitives">
      <div>
        Notice that each state employs useGlobalState individualy (XA,XB,YA,YB
        are all unique). They all have there own private useState hook. This
        means that each global state can have multiple hooks assigned to
        (reRenderFns = []). Only the setState part of the hook is used, and it
        is used for triggering re-renders. The value of the hook is never used,
        only its re-render side effect. The array of re-render functions keeps
        in sync each &quot;subscriber&quot;.
      </div>

      <Component id="A">
        <StateXA>
          <CounterA />
          <CounterB />
        </StateXA>

        <StateYA>
          <CounterA />
          <CounterB />
        </StateYA>
      </Component>

      <Component id="B">
        <StateYB>
          <CounterA />
          <CounterB />
        </StateYB>

        <StateXB>
          <CounterA />
          <CounterB />
        </StateXB>
      </Component>
    </main>
  );
}
